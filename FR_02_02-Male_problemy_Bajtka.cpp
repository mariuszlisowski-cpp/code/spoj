/*
FR_02_02 - Małe problemy Bajtka
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int calc(int);

int main() {
  int t, l, max, min;;
  cin >> t;
  while (t--) {
    cin >> l;
    max = min = l;
    for (int i = 0; i < 5; i++) {
      cin >> l;
      if (max < l) max = l;
      if (min > l) min = l;
    }
    cout << calc(min) << " " << calc(max) << endl;
  }
  return 0;
}

int calc(int num) {
  if (num == 0)
      return 1;

  int t, sum = 0;
  while(num) {
      t = num % 10;
      if (t == 0 || t == 6 || t == 9) sum++;
      if (t == 8) sum += 2;
      num /= 10;
  }
  return sum;
}
