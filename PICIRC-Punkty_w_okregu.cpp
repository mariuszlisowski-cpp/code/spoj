/*
PICIRC - Punkty w okręgu
~ oblicza odległość między dwoma punktami (i porównuje z promieniem okręgu)
*/

#include <iostream>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  float d;
  int x, xprim, y, yprim, r, t;
  cin >> x >> y >> r >> t;
  while (t--) {
    cin >> xprim >> yprim;
    d = sqrt(pow(xprim - x, 2) + pow(yprim - y, 2));
    if (d < r) cout << "I" << endl;
    if (d > r) cout << "O" << endl;
    if (d == r) cout << "E" << endl;
  }
  return 0;
}
