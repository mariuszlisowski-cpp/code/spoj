/*
JPESEL - Pesel
Sprawdż poprawność numeru PESEL
*/

#include <iostream>

using namespace std;

int main()
{
  string PESEL;
  int t, i, suma, pesel[11];
  cin >> t;

  while(t--) {
    cin >> PESEL;

    i = 11;
    while (i--){
      pesel[i] = int(PESEL[i])-48; // zamiana tekstu na tablicę cyfr
    }

    /* reguła zgodności numeru PESEL (suma kończy się cyfrą zero) */
    suma = pesel[0]*1 + pesel[1]*3 + pesel[2]*7 + pesel[3]*9 + pesel[4]*1 + pesel[5]*3 + pesel[6]*7 + pesel[7]*9 + pesel[8]*1 + pesel[9]*3 + pesel[10]*1;

    if (suma > 0) {
      if (suma % 10 == 0) // czy ostatnia cyfra jest zerem?
        cout << "D" << endl; // w porządku
      else
        cout << "N" << endl; // błędny PESEL
    }
  }

  return 0;
}
