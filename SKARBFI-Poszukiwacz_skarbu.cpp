/*
SKARBFI - SkarbFinder
*/

#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main()
{
  short t;
  cin >> t;

  while(t)
  {
    int n;
    cin >> n;
    int kierunek[n], kroki[n];

    for (int i=0; i<n; i++)
      cin >> kierunek[i] >> kroki[i];

    int x = 0, y = 0;
    for (int k=0; k<n; k++)
    {
      switch(kierunek[k])
      {
        case 0: for (int j=0; j<kroki[k]; j++) y++; // kierunek N
        break;
        case 1: for (int j=0; j<kroki[k]; j++) y--; // kierunek S
        break;
        case 2: for (int j=0; j<kroki[k]; j++) x--; // kierunek W
        break;
        case 3: for (int j=0; j<kroki[k]; j++) x++; // kierunek E
        break;
      }
    }

    /* 0 - north (N), 1 - south (S), 2 - west (W), 3 - east (E) */
    if (x == 0 && y == 0) cout << "studnia" << endl;
    if (x > 0 && y > 0)
    {
      cout << 0 << " " << y << endl; // yN
      cout << 3 << " " << x << endl; // xE
    }
    if (x < 0 && y < 0)
    {
      cout << 1 << " " << abs(y) << endl; // yS
      cout << 2 << " " << abs(x) << endl; // xW
    }
    if (x < 0 && y > 0)
    {
      cout << 0 << " " << y << endl; // yN
      cout << 2 << " " << abs(x) << endl; // xW
    }
    if (x > 0 && y < 0)
    {
      cout << 1 << " " << abs(y); // yS
      cout << 3 << " " << x; // xE
    }
    if (x == 0 && y > 0) cout << 0 << " " << y << endl; // yN
    if (x == 0 && y < 0) cout << 1 << " " << abs(y) << endl; // yS
    if (x > 0 && y == 0) cout << 3 << " " << x << endl; // yN
    if (x < 0 && y == 0) cout << 2 << " " << abs(x) << endl; // yN

    t--;
  }
  return 0;
}
