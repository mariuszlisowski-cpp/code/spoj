/*
PP0506A - Sort 1
*/

#include <iostream>
#include <cmath>

using namespace std;


int main()
{
  int t;
  cin >> t;

  while(t)
  {
    int n;
    cin >> n;
    string nazwa[n];
    int x[n], y[n];

    for (int i=0; i<n; i++)
      cin >> nazwa[i] >> x[i] >> y[i];
    for (int j=0; j<n; j++)
    {
      for (int i=0; i<n-1; i++)
      {
        double p1 = sqrt(x[i]*x[i] + y[i]*y[i]);
        double p2 = sqrt(x[i+1]*x[i+1] + y[i+1]*y[i+1]);
        if (p1 > p2)
        {
          int temp = x[i];
          x[i] = x[i+1];
          x[i+1] = temp;
          temp = y[i];
          y[i] = y[i+1];
          y[i+1] = temp;
          string temp_s = nazwa[i];
          nazwa[i] = nazwa[i+1];
          nazwa[i+1] = temp_s;
        }
      }
    }
    for (int i=0; i<n; i++)
      cout << nazwa[i] << " " << x[i] << " " << y[i] << endl;
    cout << endl;
    t--;
  }
  return 0;
}
