/*
POL - Połowa
wypisuje dokładnie pierwszą połowę podanego ciągu
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
  int t;
  cin >> t;
  string st[t];

  for (int i=0; i<t; i++)
  {
    cin >> st[i];
  }

  for (int i=0; i<t; i++)
  {
    int l = st[i].length();
    st[i].erase(l/2,l);
    cout << st[i] << endl;
  }

  return 0;
}
