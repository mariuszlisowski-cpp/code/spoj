/*
SUMA - Suma
oblicza sumę pojawiających się na wejściu liczb
*/

#include <iostream>

int main()
{
  int sum = 0, x;
  while(std::cin >> x)
    std::cout << (sum += x) << std::endl;
  return 0;
}
