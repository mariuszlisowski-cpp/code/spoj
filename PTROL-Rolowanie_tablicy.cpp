/*
PTROL - ROL
przesuwa elementy tablicy cyklicznie w lewo
*/
#include <iostream>

using namespace std;

int main()
{
  short l;
  cin >> l; // ilość testów

  while(l)
  {
    int n;
    cin >> n; // ilość liczb do przesunięcia

    int t[n];
    for (int i=0; i<n; i++)
      cin >> t[i]; // wprowadzanie tablicy

    int tmp = t[0]; // pierwszy element zachowany
    for (int i=0; i<n-1; i++)
    {
      t[i] = t[i+1]; // przesuwanie tablicy
    }
    t[n-1] = tmp; // pierwszy element teraz ostatni

    for (int i=0; i<n; i++)
      cout << t[i] << " ";

    cout << endl;
    l--;
  }
  return 0;
}
