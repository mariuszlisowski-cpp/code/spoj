/*
AL_07_01 - 0 czy 1
*/

#include <iostream>
#include <sstream>

using std::istringstream;
using std::istream;
using std::string;
using std::cout;
using std::endl;
using std::cin;

char peek_sws(istream &);
long value(istream &);
long muldiv(istream &);
long expression(istream &);

int main()
{

  int t;
  cin >> t;
  cin.ignore();
  string s;
  while (t--) {
    getline(cin, s);
    istringstream iss(s);

    if (expression(iss) % 2 == 0)
      cout << "TAK" << endl;
    else
      cout << "NIE" << endl;
  }
  return 0;
}

// ignores whitespaces and returns a printable character
char peek_sws(istream &is) {
  char c;
  while(isspace(c = is.peek()))
    is.get();
  return c;
}

// gets the first numerical value
long value(istream &is) {
  long v;
  is >> v;
  return v;
}

// multiplies and divides numbers
long muldiv(istream &is) {
  long v = value(is);

  char op; // look for the operator
  while((op = peek_sws(is)) != EOF) {
    switch(op) {
      case '*': is.get();
                v *= value(is);
                break;
      case '/': is.get();
                v /= value(is);
                break;
      default:  return v;
    }
  }
  return v;
}

// calculates the result
long expression(istream &is) {
  long v = muldiv(is);

  char op;
  while((op = peek_sws(is)) != EOF) {
    switch(op) {
      case '+': is.get();
                v += muldiv(is);
                break;
      case '-': is.get();
                v -= muldiv(is);
                break;
      default:  return v;
    };
  }
  return v;
}
