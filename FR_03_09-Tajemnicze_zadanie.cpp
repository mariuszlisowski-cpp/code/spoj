/*
FR_03_09 - Tajemnicze zadanie
*/

#include <iostream>
#include <sstream>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;

int main() {
  double* decodedFreq = new double [26];
  double* encodedFreq = new double [26];
  // odczytaj zdekodowana tablice alfabetu
  string s;
  int i = 0, l = 26;
  while (l--) {
    getline(cin, s);
    s = s.erase(0, s.find(" ")+1);
    stringstream(s) >> decodedFreq[i++];
  }
  // odczytaj zakodowany tekst
  string inputText = "";
  while (getline(cin, s))
    inputText += s + " ";
  // zlicz litery w tekscie
  int c;
  int charsNo[26] = {0}, onlyChars = inputText.size();
  for (int i = 0; i < inputText.size(); i++) {
    c = inputText[i];
    if (c > 96 && c < 123)
      charsNo[c-97]++;
    else
      if (c > 64 && c < 91)
        charsNo[c-65]++;
      else onlyChars--;
  }
  // zlicz czestotliwosci kazdej litery w tekscie
  for (int i = 0; i < 26; i++)
    encodedFreq[i] = round(((charsNo[i] / (1.0*onlyChars)) * 100000)) / 100000;
  // zdekoduj
  string outputText = "";
  for (int i = 0; i < inputText.size(); i++) {
    c = inputText[i];
    if (c >= 'a' && c <= 'z') {
      for (int j = 0; j < 26; j++)
        if (encodedFreq[c-97] == decodedFreq[j]) {
          outputText += char(97+j);
          break;
        }
    }
    else
      if (c >= 'A' && c <= 'Z') {
        for (int j = 0; j < 26; j++)
          if (encodedFreq[c-65] == decodedFreq[j]) {
            outputText += char(65+j);
            break;
          }
      }
      else
        outputText += c;
  }
  // pokaz zdekokowany tekst
  cout << outputText << endl;

  return 0;
}
