/*
KC010 - Zliczanie liczb i wyrazów
*/

#include <iostream>
#include <cctype>

using namespace std;

void licz(string, int&, int&);

int main()
{
  string s;
  int l, w;
  while (getline(cin, s)) {
    l = 0;
    w = 0;
    licz(s, l, w); // zlicza ilość liczb i wyrazów w łańcuchu
    cout << l << " " << w << endl;
  }
  return 0;
}

void licz(string aS, int& aL, int& aW) {
  string::size_type i = 0, j;
  string cut;
  while (i != aS.size()) {
    while ((i != aS.size()) && isspace(aS[i])) // szuka spacji
      i++;
    j = i;
    while ((j != aS.size()) && !isspace(aS[j])) // szuka końca wyrazu
      j++;
    if (i != j) {
      cut = aS.substr(i, j-i);
      i = j; // nowy początek do przeszukania
      if (isdigit(cut[0])) // czy pierwszy znak to cyfra
        aL++; // więc będzie to liczba (najprawdopodobniej)
      else
        aW++; // więc będzie to wyraz
    }
  }
}
