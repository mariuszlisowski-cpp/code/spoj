/*
PRZEDSZK - Przedszkolanka
najmniejsza wspólna wielokrotność: a*b/nwd(a,b)
największy wspólny dzielnik: nwd(a,b) = nwd(b, a mod b)
*/

#include <iostream>

using namespace std;

// największy wspólny dzielnik (algorytm Euklidesa)
int nwd(int aA, int aB)
{
  int c;
  while (aB !=0)
  {
    c = aA % aB;
    aA = aB;
    aB = c;
  }
  return aA;
}

int main()
{
  int t;
  cin >> t;

  int a, b, c, n;
  for (int i=0; i<t; i++)
  {
    cin >> a >> b;

    if (a == b) c = a;
    else c = (a * b) / nwd(a,b);

    cout << c << endl;
  }
  return 0;
}
