/*
POTSAM - Samolot
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  int n1, n2, k1, k2;
  cin >> n1 >> k1 >> n2 >> k2;

  cout << n1 * k1 + n2 * k2 << endl;

  return 0;
}
