/*
RNR_01_06 - Natalia w Warszawie

*/

#include <iostream>
#include <stdlib.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;

void peselCheck(int*);
void peselShow(int*);
void nestedLoops(const int, int, int*, int*);

const int peselLength = 11;
int counter = 0;

int main()
{
  //string pesel;
  string pesel = "192214?0?27";
  //string pesel = "1922140?587";
  //string pesel = "77063017739";
  int arr[peselLength];
  int pos[peselLength];

  int t = 1, count;
  //cin >> t;
  while (t--) {
    //cin >> pesel;
    count = 0;
    for (int i = 0; i < peselLength; i++) {
      if (pesel[i] != '?')
        arr[i] = pesel[i] - '0';
      else {
        pos[count] = i;
        ++count;
      }
    }

    nestedLoops(count, 10, arr, pos);
    cout << "Możliwych nr PESEL: " << counter << endl;
  }

  return 0;
}

void nestedLoops(const int n, int MAX, int* arr, int* pos) {
  int i[n+1];
  for (int a = 0; a < n+1; a++) {
    i[a] = 0;
  }

  int p = 0;
  while (i[n] == 0) {

    for (int p = 0; p < n; p++)
      arr[pos[p]] = i[p]; // iteratory n pętli jako indeksy tablicy i
    peselShow(arr);
    peselCheck(arr);

    i[0]++;
    while (i[p] == MAX) {
      i[p] = 0;
      i[++p]++;
      if (i[p]!= MAX) {
        p = 0;
      }
    }
  }
}

void peselShow(int* arr) {
  for (int i = 0; i < peselLength; i++) {
    cout << arr[i];
  }
  cout << endl;
}

void peselCheck(int* arr) {
  int sum = 0;
  for (int i = 0; i < peselLength; i++) {
    switch (i) {
      case  0: sum += arr[i] * 1; break;
      case  1: sum += arr[i] * 3; break;
      case  2: sum += arr[i] * 7; break;
      case  3: sum += arr[i] * 9; break;
      case  4: sum += arr[i] * 1; break;
      case  5: sum += arr[i] * 3; break;
      case  6: sum += arr[i] * 7; break;
      case  7: sum += arr[i] * 9; break;
      case  8: sum += arr[i] * 1; break;
      case  9: sum += arr[i] * 3; break;
      case 10: sum += arr[i] * 1; break;
    }
  }
  if (sum > 0) {
    if (sum % 10 == 0) { // czy ostatnia cyfra jest zerem?
       cout << "^^^ OK ^^^^" << endl; // poprawny PESEL
       counter++; // zliczamy poprawne
    }
    else ; // błędny PESEL
  }
}
