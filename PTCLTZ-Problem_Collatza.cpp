/*
PTCLTZ - Problem Collatza
*/

#include <iostream>

using namespace std;

int main()
{
  int t;
  cin >> t;
  int s[t];
  for (int i=0; i<t; i++)
    cin >> s[i];

  int x=0;
  for (int i=0; i<t; i++)
  {
    while (s[i] != 1)
    {
      if (s[i] % 2 == 0)
        s[i] = s[i] / 2;
      else
        s[i] = (3 * s[i]) + 1;
      x++;
    }
    cout << x << endl;
    x = 0;
  }

  return 0;
}
