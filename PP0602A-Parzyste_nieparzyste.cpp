/*
PP0602A - Parzyste nieparzyste
*/

#include <iostream>

using namespace std;

int main()
{
  int t, n;
  cin >> t;
  while (t--) {
    cin >> n;
    int arr[n];

    for (int i = 0; i < n; i++)
      cin >> arr[i];

    for (int i = 1; i <= n; i++) {
      if (i % 2 == 0)
        cout << arr[i-1] << " ";
    }

    for (int i = 1; i <= n; i++) {
      if (i % 2 != 0)
      cout << arr[i-1] << " ";
    }
    cout << endl;
  }

  return 0;
}
