/*
TRN - Transponowanie macierzy
*/

#include <iostream>

using namespace std;

int main()
{

  int m, n;
  cin >> m >> n;

  int matrix[m][n];

  for (int j = 0; j < m; j++) {
    for (int i = 0; i < n; i++) {
      cin >> matrix[j][i];
    }
  }

  for (int j = 0; j < n; j++) {
    for (int i = 0; i < m; i++) {
      cout << matrix[i][j] << " ";
    }
    cout << endl;
  }

  return 0;
}
