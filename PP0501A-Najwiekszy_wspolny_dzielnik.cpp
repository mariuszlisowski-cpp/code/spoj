/* 
PP0501A - NWD
wyznacza największy wspólny dzielnik dwóch liczb
*/

#include <iostream>

using namespace std;

int nwd(int, int);

int main()
{
  int t;
  cin >> t;

  int a, b;
  for (int i=0; i<t; i++)
  {
    cin >> a >> b;
    cout << nwd(a,b) << endl;
  }

  return 0;
}

// największy wspólny dzielnik (algorytm Euklidesa)
int nwd(int aA, int aB)
{
  int c;
  while (aB !=0)
  {
    c = aA % aB;
    aA = aB;
    aB = c;
  }
  return aA;
}
