/*
KC004 - Zliczanie wystąpień
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  int l, d, j;
  while (cin >> l >> d) {
    int c[d];
    j = 0;
    for (int i = 0; i < d; i++) {
      cin >> c[i];
      if (c[i] == l) j++; // szukamy l w tablicy c
    }
    cout << j << endl;
  }
  return 0;
}
